# IPTABLES configuración avanzada

## Redirección de puertos DNAT

Petición de una web a la ip pública del router 5.6.7.8:8080 
que se ha de redirigir a la ip interna del servidor web 192.168.5.3:80

La cadena PREROUTING es dstnat, porque puedo cambiar el dst (destionation)
La cadena POSTROUTING es srcnat, porque puedo cambiar el src (source)

La condición que usaremos es que cualquier petición que venga hacia el
puerto 8080 independientemente de ips, puertos de entrada...

```
/ip firewall nat add chain=dstnat \ # CADENA:    dstnat (PREROUTING)
                     protocol=tcp \ # CONDICIÓN: que el protocolo sea tcp
                     dst-port=8080 \  # CONDICIÓN: que el puerto de destino sea el 8080
                     action=dst-nat \ #  ACCIÓN: cambiar el destino
                     to-addresses=192.168.5.3 \ # ACCIÓN: al puerto 192.168.5.3
                     to-ports=80                # ACCIÓN: al puerto 80

```

## Enmascaramiento usando masquerade y srcnat

El enmascaramiento se suele configurar usando masquerade. Pero con esta
acción no podemos especificar la ip origen que queremos usar para enmascarar

Con la regla srcnat tenemos más control sobre como realizar este enmascaramiento

Ejemplo de regla con masquerade:

```
/ip firewall nat 
add chain=srcnat out-interface=eth4 action=masquerade
```

Ejemplo de regla con srcant:

```
/ip firewall nat 
add chain=srcnat out-interface=eth4 action=src-nat to-addresses=192.168.1.254
```
                     
## Ejemplo de reglas de filtrado, el orden importa

Solo queremos que los paquetes que vengan de la red 10.5.3.0/24 puedan
tener como destino las ips privadas:

Iptables son ACL (Access Control List): Si se cumple una condición se 
ejecuta la acción y no se miran más reglas, si no, se pasa a la siguiente regla

```
/ip firewall filter
add chain=forward dst-address=10.0.0.0/8 src-address=10.5.3.0/24
add chain=forward dst-address=172.16.0.0/12 src-address=10.5.3.0/24
add chain=forward dst-address=192.168.0.0/16 src-address=10.5.3.0/24
add action=drop chain=forward src-address=10.5.3.0/24

```

## Condiciones avanzadas: limitando en función de la hora de conexión

Se puede consultar el manual de mikrotik para conocer 
todas las opciones de filtrado: 
[https://wiki.mikrotik.com/wiki/Manual:IP/Firewall/Filter#Properties]

Ejemplo de regla de firewall para que sólo deje conectarse a los equipos
de la familia 192.168.3.0/24 de 10h a 12h

```
/ip firewall filter
add chain=forward src-address=192.168.3.0/24 time=\
    10h-12h,mon,tue,wed,thu,fri action=accept
add chain=forward src-address=192.168.3.0/24 action=drop

```


## Mangle: marcado de paquetes

Existen 3 tipos de marcas:
- mark-connection: Sirve para marcar una conexión. Una vez se pone la marca
esta marca queda asociada a todos los paquetes que siguen esa conexión. 
- mark-routing: Sirve para marcar los paquetes y que los pueda interpretar
la tabla de routing. Sólo es válida mientras el paquete atraviesa las cadenas
del iptables, una vez sale de la interfaz se pierde, no es como las marcas
de conexión
- mark-packet: Sirve para marcar los paquetes y que los puedan usar otras cadenas
o bien en las queues (colas) de calidad de servicio (Q0S)

Vamos a marcar las conexiones que van al servidor donde me voy a descargar
la iso de la distribución gentoo. Este mirror desde donde se descarga
pertenece a rediris, que es una institución española que puede tener 
un rango de ips reservado:

http://ftp.rediris.es/mirror/Gentoo-Releases/amd64/12.1/livedvd-amd64-multilib-2012.1.iso



Consultamos esta dirección que ip tiene:

```
$ host ftp.rediris.es
ftp.rediris.es is an alias for zeppo.rediris.es.
zeppo.rediris.es has address 130.206.1.5
```

Para conocer que ips tiene la institución hacemos un whois:

```

$ whois 130.206.1.5
[...]
inetnum:        130.206.0.0 - 130.206.255.255
netname:        REDIRIS
org:            ORG-RA6-RIPE
descr:          RedIRIS
descr:          Spanish National R&D Network
descr:          Madrid, Spain
[...]
```

Y comprobamos que tienen una clase B (/16) reservada. Filtraremos entonces
por toda esa clase B, ya que si en un futuro la dirección ip del servidor
varía o es balanceada, lo hará dentro de ese rango de ips seguramente.


```
/ip firewall mangle 
add chain=prerouting            \#    CADENA: prerouting 
    dst-address=130.206.0.0/16  \# CONDICIÓN: ip destino sea de rediris
    action=mark-connection      \#    ACCIÓN: marcar la conexión
    new-connection-mark=rediris \#    ACCIÓN: la marca de la conexión será rediris
    

add chain=prerouting            \#    CADENA: prerouting  
    connection-mark=rediris     \# CONDICIÓN: si la marca de conexión es rediris
    action=mark-packet          \#    ACCIÓN: aplica una marca de paquetes 
    new-packet-mark=limita-rediris \# ACCIÓN: la marca de paquete será limita-rediris
                                            # que podremos consultar en Queues de QoS
                                            
                                            
add chain=prerouting            \#    CADENA: prerouting 
    connection-mark=rediris     \# CONDICIÓN: si la marca de conexión es rediris
    action=mark-routing         \#    ACCIÓN: aplica una marca de routing 
    new-routing-mark=ruta-a-rediris \#ACCIÓN: la marca de routing será ruta-a-rediris
                                            # que podremos consultar para aplicar
                                            # unas reglas de una tabla de routing distinta
                                            # (policy routing)

```
