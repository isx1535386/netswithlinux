BRIDGES Y ROUTERS

##### 1. Conexionado, nombres de interfaces y direcciones MAC

Para esta práctica hay que interconectar los equipos de una fila con 2 latiguillos de red (sin usar las rosetas del aula) siguiendo el siguiente esquema:

```
FILA DE 3 EQUIPOS:
                              |usbB                          
   +--------+            +--------+            +--------+   
   |        |eth0    usbA|        |usbC    eth0|        |   
   | HOST A +------------+ HOST B +------------+ HOST C |   
   |        |            |        |            |        |   
   +--------+            +--------+            +--------+   
                             |eth0                          
```

El equipo del medio de la fila ha de tener conectadas 3 tarjetas usb-ethernet. Se han de renombrar estas tarjetas y cambiar las mac. Las direcciones mac de cada tarjeta han de ser distintas siguiendo la siguiente
codificación:  

AA:AA:AA:00:00:YY
```
ip link set usb0 down
ip link set usb0 name usbA
ip link set usbA addres aa:aa:aa:00:00:09
ip link set usbA up

ip link set usbB down
ip link set usbB addres aa:aa:aa:00:00:08
ip link set usbB up

ip link set eth0 down
ip link set eth0 name usbC
ip link set usbC addres aa:aa:aa:00:00:07
ip link set usbC up
```


Siendo YY la numeración de la tarjeta USB que lleva en su pegatina.

Hay que renombrar las tarjetas de red para que se llamen: usbA, usbB, usbC y eth0(la de la placa base)

ip link set eth0 down
ip link set eth0 name usbA
ip link set usbA up
ip a

	7: usbA: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::14a4:deca:8bf4:8c8f/64 scope link 
       valid_lft forever preferred_lft forever
l

# ip link set usb0 down
[root@j08 ~]# ip link set usb0 name usbC
[root@j08 ~]# ip link set usbC up
[root@j08 ~]# ip a

usbC: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
 
[root@j08 ~]# ip link set usb0 name usbC
[root@j08 ~]# ip link set usbC up
[root@j08 ~]# ip a   
 
 usbB: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff

[root@j08 ~]# ip link set enp2s0 down
[root@j08 ~]# ip link set enp2s0 name eth0
[root@j08 ~]# ip link set eth0 up

[root@j08 ~]# ip link
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN mode DEFAULT group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
2: eth0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
    link/ether 14:da:e9:99:a1:78 brd ff:ff:ff:ff:ff:ff
7: usbA: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether aa:aa:aa:00:00:09 brd ff:ff:ff:ff:ff:ff
8: usbC: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP mode DEFAULT group default qlen 1000
    link/ether aa:aa:aa:00:00:07 brd ff:ff:ff:ff:ff:ff
9: usbB: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN mode DEFAULT group default qlen 1000
    link/ether aa:aa:aa:00:00:08 brd ff:ff:ff:ff:ff:ff


Al final la orden ip link show ha de mostrar algo como lo siguiente:



##### 2. Conectar 3 equipos haciendo routing y saliendo a internet:

A. Configurar las ips en cada equpo según el esquema y hacer pings desde hostA a hostB y desde hostC a hostA

```
ESQUEMA CAPA 3
                       +---------+
    172.17.9.0/24    .1| ROUTER  |.1   172.17.7.0/24
   +-------------------+ HOST B  +--------------------+
        |.2        usbA|         |usbC        |.2
        |eth0          +---------+            |eth0
     +------+                              +------+
     |HOST A|                              |HOST C|
     +------+                              +------+
                            
                            

```
	el ma
	per al host B: 172.17.9.1	  172.17.9.1/24


B. Activar el bit de forwarding y poner como default gateway al host B que hará de router entre las dos redes, listar las rutas en host B y verificar que se pueden hacer pings entre HOST A y HOST C
echo 1 > /proc/sys/net/ipv4/ip_forward
C. Conectar eth0 del HOST B a la roseta del aula, pedir una ip dinámica con dhclient. Verificar que desde el HOST B se puede hacer ping al 8.8.8.8. 

	dhclient
	
	 dhclient
[root@j08 ~]# ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
    inet6 ::1/128 scope host 
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:78 brd ff:ff:ff:ff:ff:ff
    inet 192.168.3.8/16 brd 192.168.255.255 scope global dynamic eth0
       valid_lft 21656sec preferred_lft 21656sec
3: usbB: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether aa:aa:aa:aa:00:08 brd ff:ff:ff:ff:ff:ff
    inet6 fe80::a8aa:aaff:fe00:808/64 scope link 
       valid_lft forever preferred_lft forever
16: usbA: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether aa:aa:aa:aa:00:09 brd ff:ff:ff:ff:ff:ff
    inet 172.17.9.1/24 scope global usbA
       valid_lft forever preferred_lft forever
17: usbC: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether aa:aa:aa:aa:00:07 brd ff:ff:ff:ff:ff:ff
    inet 172.17.7.1/24 scope global usbC
       valid_lft forever preferred_lft forever



fer un ping a internet
[root@j08 ~]# ping 8.8.8.8
PING 8.8.8.8 (8.8.8.8) 56(84) bytes of data.
64 bytes from 8.8.8.8: icmp_seq=2 ttl=55 time=11.0 ms
64 bytes from 8.8.8.8: icmp_seq=4 ttl=55 time=11.2 ms



D. Activar el enmascarmiento de ips para el tráfico saliente por eth0 y verificar que HOST A Y HOST C pueden hacer ping al 8.8.8.8


	El router hace lo siguiente: (enmascarar):
	
	[root@j08 ~]# iptables -t nat -A POSTROUTING -o eth0 -j MASQUERADE

E. Poner como servidor dns a 8.8.8.8 en /etc/resolv.conf en hostA y host C y verificar que se puede navegar por internet con  haciendo un wget de http://www.netfilter.org

FER esto amb el host A i C+

echo "nameserver 8.8.8.8" > /etc/resolv.conf
Com

##### 3. Conectar 2 equipos utilizando un equipo intermedio que hace de bridge. 

En esta práctica queremos que HOST B trabaje como si fuera un switch, interconectando a host A y host C. 

A. Hacer flush de todas las ips y verificar que no queda ninguna ruta ni dirección ip asociada a ningún equipo.
```
ip r f all

```
B. Crear el bridge br0 y añadir usbA y usbC a ese bridge. Listar con detalle la configuración del bridge.
```
[root@j08 ~]# brctl addbr br0
[root@j08 ~]# brctl addif br0 usbA
[root@j08 ~]# brctl addif br0 usbC

[root@j08 ~]# brctl show
bridge name	bridge id		STP enabled	interfaces
br0		8000.aaaaaa000007	no		usbA
							usbC
```
C. En el host A nos ponemos la ip 192.168.100.A/24 y en el equipo C la ip 192.168.100.B/24. Hacemos ping entre los dos equipos y debería de ir.
```
ip a a 192.168.100.7/24 dev eth0 ---> per al equip C
ip a a 192.168.100.9/24 dev eth0 ---> per al equip A
```
D. Listar en host B la tabla de relación de puertos y MACs en el bridge
```
[root@j08 ~]# brctl showmacs br0
port no	mac addr		is local?	ageing timer
  2	14:da:e9:b6:e5:13	no		   0.18
  1	14:da:e9:b6:e9:07	no		   0.17
  2	aa:aa:aa:00:00:07	yes		   0.00
  2	aa:aa:aa:00:00:07	yes		   0.00
  1	aa:aa:aa:00:00:09	yes		   0.00
  1	aa:aa:aa:00:00:09	yes		   0.00
```

##### 4. Conectar todos los equipos del aula haciendo switching.

Conseguir esta interconexión entre las filas:
```
\
                   |                     
                   +------+              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+              
 +------+ +------+        |              
                          |              
                          |              
  BRIDGE : br0            |              
 +-------------------------------------+ 
 | +---+  +---+  +---+  +---+          | 
 | | 1 |  | 2 |  | 3 |  | 4 |   HOST B | 
 | +---+  +---+  +---+  +---+          | 
 |   |usbA  |usbC  |eth0   usbB        | 
 +-------------------------------------+ 
     |      |      |                     
     |      |      |                     
     |      |      |                     
 +---+--+ +-+----+ |                     
 |HOST A| |HOST C| +------+
 +------+ +------+        |
                          |
                          +
```
Cada equipo ha de tener una ip 192.168.100.B/24

A. Lanzar fpings para verificar que todos los equipos A y C responden

B. Asignar una ip al br0 del hostB y lanzar fping para verificar que todos los equipos A,B y C responden

C. Listar la tabla de asignación de puertos y MACs después de hacer un fping 

##### 5. Conectar todos los equipos del aula haciendo routing.

```
\
                         +
                         |
                         |
                         |
                     +---+-----+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2            |         |          .2|
      |              +---------+            |
   +--+---+              |.1             +--+---+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |172.16.XX.0/24
                         |
                         |
                         |
                     usbB|.2
                     +---------+
  172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
 +-------------------+ HOST B  +--------------------+
      |.2        usbA|         |usbC        |.2
      |eth0          +---------+            |eth0
   +------+          eth0|.1             +------+
   |HOST A|              |               |HOST C|
   +------+              |               +------+
                         |
                         |
                         |
                         +
```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden

##### 6. Conectar todos los equipos del aula haciendo switching y routing.

Seguir este esquema


```
                                       172.16.0.0/24
           +------------------------------------------------------------------------------+
                        |.XX                                                     |.XX
                        |                                                        |
                        |br0(usbB,eth0)                                          |br0(usbB,eth0)
                    +---------+                                              +---------+
 172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24        172.17.XX.0/24   .1| ROUTER  |.1   172.17.XX.0/24
+-------------------+ HOST B  +--------------------+     +-------------------+ HOST B  +--------------------+
     |.2        usbA|         |usbC        |.2                |.2        usbA|         |usbC        |.2
     |eth0          +---------+            |eth0              |eth0          +---------+            |eth0
  +------+                              +------+           +------+                              +------+
  |HOST A|                              |HOST C|           |HOST A|                              |HOST C|
  +------+                              +------+           +------+                              +------+

```

A. Introducir todas las rutas necesarias en el host B y listarlas

B. Lanzar fping para verificar que todos los equipos A, B y C responden
