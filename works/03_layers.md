#### 1. ip a

Borra todas las rutas y direcciones ip, para el servicio NetworkManager y asegúrate que no queda ningún demonio de dhclient corriendo. Comprueba que no queda ninguna con "ip a" y "ip r"

	ip r f all
	ip a f dev enp2s0
	ip a----no sale direccion
	ip r ----no sale ninguna ruta
	
	2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:78 brd ff:ff:ff:ff:ff:ff
	
Ponte las siguientes ips en tu tarjeta ethernet: 
2.2.2.2/24, 
3.3.3.3/16, 
4.4.4.4/25

	ip a a 2.2.2.2/24 dev enp2s0
	ip a a 3.3.3.3/16 dev enp2s0
	ip a a 4.4.4.4/25 dev enp2s0
	ip a s dev enp2s0
	
	 enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:78 brd ff:ff:ff:ff:ff:ff
    inet 2.2.2.2/24 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 3.3.3.3/16 scope global enp2s0
       valid_lft forever preferred_lft forever
    inet 4.4.4.4/25 scope global enp2s0
       valid_lft forever preferred_lft forever

	
Consulta la tabla de rutas de tu equipo
	
	ip r
	2.2.2.0/24 dev enp2s0  proto kernel  scope link  src 2.2.2.2 
	3.3.0.0/16 dev enp2s0  proto kernel  scope link  src 3.3.3.3 
	4.4.4.0/25 dev enp2s0  proto kernel  scope link  src 4.4.4.4 

Haz ping a las siguientes direcciones y justifica por qué en algunas sale el mensaje de "Network is unrecheable", en otras contesta y en otras se queda esperando sin dar mensajes de error:

2.2.2.2 , 2.2.2.254 , 2.2.5.2 , 3.3.3.35 , 3.3.200.45 , 4.4.4.8, 4.4.4.132

	EL PRIMER ESTA BE PERQUE CONCIDE CON LAS DEMAS y EXISTE

	ping 2.2.2.2
	PING 2.2.2.2 (2.2.2.2) 56(84) bytes of data.
	64 bytes from 2.2.2.2: icmp_seq=1 ttl=64 time=0.035 ms
	
	Perque en el ultim byte canvia la ultima part 
	
	ping 2.2.2.254
	PING 2.2.2.254 (2.2.2.254) 56(84) bytes of data.
	From 2.2.2.2 icmp_seq=1 Destination Host Unreachable

	No encuentra la direccion ip buscada perque no esta dintre de la mascara
	
	ping 2.2.5.2
	connect: Network is unreachable
	
	
	ping 3.3.3.35
	PING 3.3.3.35 (3.3.3.35) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	
	ping 3.3.200.45
	PING 3.3.200.45 (3.3.200.45) 56(84) bytes of data.
	From 3.3.3.3 icmp_seq=1 Destination Host Unreachable
	
	La ip no escucha que no existe
	
	ping 4.4.4.8
	PING 4.4.4.8 (4.4.4.8) 56(84) bytes of data.
	From 4.4.4.4 icmp_seq=1 Destination Host Unreachable
	
	ping 4.4.4.132
	connect: Network is unreachable
	


#### 2. ip link

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	ip a f dev enp2s0
	ip r f all

Conecta una segunda interfaz de red por el puerto usb

	usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
    
Cambiale el nombre a usb0

Modifica la dirección MAC

Asígnale la direcció ip 5.5.5.5/24 a usb0 y 7.7.7.7/24 a la tarjeta de la placa base.

	ip a a 5.5.5.5/24 dev usb0
	ip a a 7.7.7.7/24 dev enp2s0
	
     2: enp2s0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 14:da:e9:99:a1:78 brd ff:ff:ff:ff:ff:ff
    inet 7.7.7.7/24 scope global enp2s0
       valid_lft forever preferred_lft forever
	4: usb0: <NO-CARRIER,BROADCAST,MULTICAST,UP> mtu 1500 qdisc fq_codel state DOWN group default qlen 1000
    link/ether 00:e0:4c:53:44:58 brd ff:ff:ff:ff:ff:ff
    inet 5.5.5.5/24 scope global usb0
       valid_lft forever preferred_lft forever

Observa la tabla de rutas

	ip r
	
	$5.5.5.0/24 dev usb0  proto kernel  scope link  src 5.5.5.5 linkdown 
	$7.7.7.0/24 dev enp2s0  proto kernel  scope link  src 7.7.7.7 


#### 3. iperf

Borra todas las rutas y direcciones ip de la tarjeta ethernet

	ip r f all

En cada ordenador os ponéis la ip 172.16.99.XX/24 (XX=puesto de trabajo)

	ip a a 172.16.99.8/24 dev enp2s0
	
Lanzar iperf en modo servidor en cada ordenador

		iperf -s
	------------------------------------------------------------
	Server listening on TCP port 5001
	TCP window size: 85.3 KByte (default)
	------------------------------------------------------------

Comprueba con netstat en qué puerto escucha

	netstat -utlnp
	
	Active Internet connections (only servers)
	Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
	tcp        0      0 0.0.0.0:5001            0.0.0.0:*               LISTEN      3055/iperf          


Conectarse desde otro pc como cliente

	iperf -c 172.16.99.8/24
	

Repetir el procedimiento y capturar los 30 primeros paquetes con tshark

Encontrar los 3 paquetes del handshake de tcp

Abrir dos servidores en dos puertos distintos

Observar como quedan esos puertos abiertos con netstat

Conectarse al servidor con dos clientes y que la prueba dure 1 minuto

Mientras tanto con netstat mirar conexiones abiertas

