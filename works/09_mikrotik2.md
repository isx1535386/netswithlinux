EJERCICIO MIKROTIK 2.

OBJETIVO: CREAR DOS REDES WIFIS CON DISTINTOS TIPOS DE SEGURIDAD


 MIKROTIK
 +-------------------+
 |  1  2  3  4  wifi |
 +-------------------+
    |  |  |  |
    |  |  |  |
    |  |  |  +
    |  |  |  Conexión a un switch  con vlans
    |  |  +
    |  |  Interface de red placa base
    |  +
    |  Interface USB
    |  Ip 192.168.88.1XX/24
    +
    Punto de red del aula
    para salida a internet

## Pasos para conseguir tener 2 redes wifis separadas con distintos
niveles de seguridad.

Recordamos que:
- Solo disponemos de una interface wifi que trabaja en la banda de 2,4GHz.
- Este modelo de mikrotik dispone de 4 interfaces ethernet y 1 interface wifi
- En el PC dispondremos de una interface USB que es la que usaremos para
configurar el router por el puerto 2

Para conseguir dos redes wifi a partir de una sola interface hay que crear
sub-interfaces o interfaces virtuales. Cada interface virtual ha de estar 
asociada a un id de vlan (número de 1 a 2048)

Si queremos comunicarnos en la misma red entre un dispositivo wifi y un dispositivo
cableado hay que crear bridges entre la interface wifi y la interface ethernet.

### Eliminar configuración inicial
La configuración inicial que viene con la mikrotik podemos consultarla haciendo 
/export y obtenmos:

```
/interface bridge
add admin-mac=6C:3B:6B:30:B8:4F auto-mac=no comment=defconf name=bridge
/interface wireless
set [ find default-name=wlan1 ] band=2ghz-b/g/n channel-width=20/40mhz-Ce \
    disabled=no distance=indoors frequency=auto mode=ap-bridge ssid=\
    MikroTik-30B852 wireless-protocol=802.11
/interface ethernet
set [ find default-name=ether2 ] name=ether2-master
set [ find default-name=ether3 ] master-port=ether2-master
set [ find default-name=ether4 ] master-port=ether2-master
/ip neighbor discovery
set ether1 discover=no
set bridge comment=defconf
/ip pool
add name=default-dhcp ranges=192.168.88.10-192.168.88.254
/ip dhcp-server
add address-pool=default-dhcp disabled=no interface=bridge name=defconf
/interface bridge port
add bridge=bridge comment=defconf interface=ether2-master
add bridge=bridge comment=defconf interface=wlan1
/ip address
add address=192.168.88.1/24 comment=defconf interface=bridge network=\
    192.168.88.0
/ip dhcp-client
add comment=defconf dhcp-options=hostname,clientid disabled=no interface=ether1
/ip dhcp-server network
add address=192.168.88.0/24 comment=defconf gateway=192.168.88.1
/ip dns
set allow-remote-requests=yes
/ip dns static
add address=192.168.88.1 name=router
/ip firewall filter
add chain=input comment="defconf: accept ICMP" protocol=icmp
add chain=input comment="defconf: accept established,related" connection-state=\
    established,related
add action=drop chain=input comment="defconf: drop all from WAN" in-interface=\
    ether1
add action=fasttrack-connection chain=forward comment="defconf: fasttrack" \
    connection-state=established,related
add chain=forward comment="defconf: accept established,related" \
    connection-state=established,related
add action=drop chain=forward comment="defconf: drop invalid" connection-state=\
    invalid
add action=drop chain=forward comment="defconf:  drop all from WAN not DSTNATed" \
    connection-nat-state=!dstnat connection-state=new in-interface=ether1
/system routerboard settings
set boot-device=flash-boot cpu-frequency=650MHz protected-routerboot=disabled
/tool mac-server
set [ find default=yes ] disabled=yes
add interface=bridge
/tool mac-server mac-winbox
set [ find default=yes ] disabled=yes

```

A continuación modificamos todas esas partes de la configuración que no nos
interesan para dejar la mikrotik pelada, sin configuraciones iniciales
que interfieran con nuestra nueva programación del router:

```
# Cambiamos dirección IP para que la gestione directamente la interface 2
ip address set 0 interface=ether2-master

# Eliminamos el bridge

/interface bridge port remove 1
/interface bridge port remove 0
/interface bridge remove 0  

# Eliminamos que un puerto sea master de otro

/interface ethernet set [ find default-name=ether3 ] master-port=none
/interface ethernet set [ find default-name=ether4 ] master-port=none

# Cambiamos nombres a los puertos

/interface ethernet set [ find default-name=ether1 ] name=eth1
/interface ethernet set [ find default-name=ether2 ] name=eth2
/interface ethernet set [ find default-name=ether3 ] name=eth3
/interface ethernet set [ find default-name=ether4 ] name=eth4

# Deshabilitamos la wifi

/interface wireless set [ find default-name=wlan1 ] disabled=yes

# Eliminamos servidor y cliente dhcp

/ip pool remove 0
/ip dhcp-server network remove 0
/ip dhcp-server remove 0
/ip dhcp-client remove 0
/ip dns static remove 0
/ip dns set allow-remote-requests=no

# Eliminamos regla de nat masquerade
/ip firewall nat remove 0

```

Ahora le cambiamos el nombre y guardamos la configuración inicial por
si la necesitamos restaurar en cualquier momento
```
/system identity set name=mkt08
/system backup save name="20170321_zeroconf"
```


###[ejercicio1] Explica el procedimiento para resetear el router y restaurar
este backup, verifica con /export que no queda ninguna configuración inicial 
que pueda molestarnos para empezar a programar el router

[admin@infMKT08] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
[admin@infMKT08] > /system identity set name=mkt08
[admin@mkt08] > /system backup save name="20170317_zeroconf"

##--resetamos para poder volver a cargar el backup con la siguiente ordre

[admin@mkt08] > system reset-configuration     
Dangerous! Reset anyway? [y/N]: 
y
system configuration will be reset
Connection to 192.168.88.1 closed by remote host.
Connection to 192.168.88.1 closed.
[user1@j08 netswithlinux]$ ssh admin@192.168.88.1

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the RSA key sent by the remote host is
SHA256:Eb/bpBSsR7/Tqy4BnPNYrKYnNW8It/48mWh8eBKpzI0.
Please contact your system administrator.
Add correct host key in /tmp/user1/.ssh/known_hosts to get rid of this message.
Offending RSA key in /tmp/user1/.ssh/known_hosts:2
RSA host key for 192.168.88.1 has changed and you have requested strict checking.
Host key verification failed.

NOTA:al resetar el MKT los valores puestos regresan al los de predefinidos
por el router(como se puede ver en la siguiente orden):

[admin@MikroTik] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2   S ether3                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:BC:BF:A2

---buscamos el nombre del bakup que cambiamos

[admin@MikroTik] > file print  
 # NAME                                                 TYPE                                                      SIZE CREATION-TIME       
 0 skins                                                directory                                                      jan/01/1970 00:00:01
 1 auto-before-reset.backup                             backup                                                 24.6KiB jan/02/1970 01:06:53
 2 autosupout.rif                                       .rif file                                             446.7KiB jan/02/1970 00:24:21
 3 20170317_zeroconf.backup                             backup                                                 24.6KiB jan/02/1970 01:04:21

--Ahora cargamos el bakup que guardamos antes.

[admin@MikroTik] > system backup load name=20170317_zeroconf.backup 
password: *****
Restore and reboot? [y/N]: 
y
Restoring system configuration
System configuration restored, rebooting now
Connection closed by foreign host.
[user1@j08 netswithlinux]$ ssh admin@192.168.88.1
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@    WARNING: REMOTE HOST IDENTIFICATION HAS CHANGED!     @
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
IT IS POSSIBLE THAT SOMEONE IS DOING SOMETHING NASTY!
Someone could be eavesdropping on you right now (man-in-the-middle attack)!
It is also possible that a host key has just been changed.
The fingerprint for the RSA key sent by the remote host is
SHA256:Eb/bpBSsR7/Tqy4BnPNYrKYnNW8It/48mWh8eBKpzI0.
Please contact your system administrator.
Add correct host key in /tmp/user1/.ssh/known_hosts to get rid of this message.
Offending RSA key in /tmp/user1/.ssh/known_hosts:2
RSA host key for 192.168.88.1 has changed and you have requested strict checking.
Host key verification failed.
[user1@j08 netswithlinux]$ telnet 192.168.88.1


NOTA:al volverlo a cargar cambia de calve i tenemo0s que borrar la anterior para
que no nos moleste (i haci cada vez que hagamos el backup):

[user1@j08 netswithlinux]$ rm -f ~/.ssh/known_hosts 

---los valores del bakup se restauren

[admin@mkt08] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5



### Crear interfaces y bridges

Primero hay que pensar en capa 2, vamos a crear un bridge por cada red wifi
que tenga un punto de red cableado asociado a una vlan

Trabajaremos con dos vlans:

*1XX: red pública
*2XX: red privada

Hay que crear unas interfaces virtuales wifi adicional y cambiar los ssids
para reconocerlas cuando escaneemos las wifis

```
/interface wireless set 0 ssid="free1XX"
/interface wireless add ssid="private2XX" master-interface=wlan1
```

admin@mkt08] > interface print          
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4  X  wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5  X  wlan2                               wlan                   1600       2290 6E:3B:6B:BC:BF:A5



###[ejercicio2] Explica por qué aparecen las interfaces wifi como disabled
al hacer /interface print. Habilita y deshabilita estar interfaces. Cambiales
el nombre a wFree wPrivate y dejalas deshabilitadas

---HAbilitar les wifis
[admin@mkt08] > interface wireless enable wlan1 
[admin@mkt08] > interface wireless enable wlan2 

[admin@mkt08] > interface print                
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4     wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5     wlan2                               wlan             1500  1600       2290 6E:3B:6B:BC:BF:A5

-cambiar el nombre de las wifi
[admin@mkt08] > interface set name="wFree" wlan1
[admin@mkt08] > interface set name="wPrivate" wlan2 
[admin@mkt08] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2     eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4     wFree                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5     wPrivate                            wlan             1500  1600       2290 6E:3B:6B:BC:BF:A5

## Crear interfaces virtuales y hacer bridges

```
/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4   

/interface bridge add name=br-vlan1XX
/interface bridge add name=br-vlan2XX


/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
/interface bridge port add interface=eth3 bridge=br-vlan1XX
/interface bridge port add interface=wFree  bridge=br-vlan1XX      


/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX
/interface bridge port add interface=wPrivate   bridge=br-vlan2XX

/interface print


```

### [ejercicio3] Comenta cada una de estas líneas de la configuración
anterior para que quede bien documentado

[admin@mkt08] > /interface print
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     eth1                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  R  eth2                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2   S eth3                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3     eth4                                ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4   S wFree                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5   S wPrivate                            wlan             1500  1600       2290 6E:3B:6B:BC:BF:A5
 6  R  br-vlan108                          bridge           1500  1594            6C:3B:6B:BC:BF:A4
 7  R  br-vlan208                          bridge           1500  1594            6C:3B:6B:BC:BF:A4
 8   S eth4-vlan108                        vlan             1500  1594            6C:3B:6B:BC:BF:A4
 9   S eth4-vlan208                        vlan             1500  1594            6C:3B:6B:BC:BF:A4


---añadimos les vlans con su respectivo nombre i interifcie en cada puerto
---habilitamos las interfaces vlans.
```
/interface vlan add name eth4-vlan1XX vlan-id=1XX interface=eth4
/interface vlan add name eth4-vlan2XX vlan-id=2XX interface=eth4   
```
---creamos los puentes bridgets para cada 
---vlan(la publica i privada)

```
/interface bridge add name=br-vlan1XX
/interface bridge add name=br-vlan2XX
```
---añadimos la interfaz publica que forman el puente con las interficies
ya creadas antes   
```
/interface bridge port add interface=eth4-vlan1XX bridge=br-vlan1XX
/interface bridge port add interface=eth3 bridge=br-vlan1XX
/interface bridge port add interface=wFree  bridge=br-vlan1XX      
```
---añadimos la interfaz privada que forman el puente con las interficies
ya creadas antes
```
/interface bridge port add interface=eth4-vlan2XX bridge=br-vlan2XX
/interface bridge port add interface=wPrivate   bridge=br-vlan2XX
```


### [ejercicio4] Pon una ip 172.17.1XX.1/24 a br-vlan1XX 
y 172.17.2XX.1/24 a br-vlan2XX y verifica desde la interface de la placa 
base de tu ordenador que hay conectividad (puedes hacer ping) configurando
unas ips cableadas. Cuando lo hayas conseguido haz un backup de la configuración

[admin@mkt08] > ip address add interface=br-vlan108 address=172.17.108.1/24
[admin@mkt08] > ip address add interface=br-vlan208 address=172.17.208.1/24

[admin@mkt08] > ip address print                                           
Flags: X - disabled, I - invalid, D - dynamic 
 #   ADDRESS            NETWORK         INTERFACE                                                                                          
 0   ;;; defconf
     192.168.88.1/24    192.168.88.0    eth2                                                                                               
 1   172.17.108.1/24    172.17.108.0    br-vlan108                                                                                         
 2   172.17.208.1/24    172.17.208.0    br-vlan208                                                                                         
[admin@mkt08] > 


[admin@mkt08] > /interface bridge port print 
Flags: X - disabled, I - inactive, D - dynamic 
 #    INTERFACE                                            BRIDGE                                            PRIORITY  PATH-COST    HORIZON
 0 I  eth4-vlan108                                         br-vlan108                                            0x80         10       none
 1 I  eth3                                                 br-vlan108                                            0x80         10       none
 2 I  wFree                                                br-vlan108                                            0x80         10       none
 3 I  eth4-vlan208                                         br-vlan208                                            0x80         10       none
 4 I  wPrivate                                             br-vlan208                                            0x80         10       none


--posem una ip a la familia de :
---#ip a a 172.17.120.20/24 dev enp2s0
---#ip a a 172.17.220.40/24 dev enp2s0


	#ping 172.17.120.20
	
	###Hace ping a la 172.17.120.20 del puert 3:

	[root@j08 ~]# ping 172.17.120.20
	PING 172.17.120.20 (172.17.120.20) 56(84) bytes of data.
	64 bytes from 172.17.120.20: icmp_seq=1 ttl=64 time=0.055 ms
	64 bytes from 172.17.120.20: icmp_seq=2 ttl=64 time=0.057 ms
	64 bytes from 172.17.120.20: icmp_seq=3 ttl=64 time=0.066 ms
	64 bytes from 172.17.120.20: icmp_seq=4 ttl=64 time=0.068 ms
	64 bytes from 172.17.120.20: icmp_seq=5 ttl=64 time=0.061 ms
	64 bytes from 172.17.120.20: icmp_seq=6 ttl=64 time=0.040 ms
	
	##No hace ping a la 172.17.220.20
	
	root@j08 ~]# ping 172.17.220.20
	PING 172.17.220.20 (172.17.220.20) 56(84) bytes of data.
	From 172.17.220.40 icmp_seq=1 Destination Host Unreachable
	From 172.17.220.40 icmp_seq=2 Destination Host Unreachable
	From 172.17.220.40 icmp_seq=3 Destination Host Unreachable
	From 172.17.220.40 icmp_seq=4 Destination Host Unreachable
	
	 


### [ejercicio5] Crea una servidor dhcp para cada una de las redes en los rangos
.101 a .250 de las respectivas redes. Asignar ip manual a la eth1. 
Crear reglas de nat para salir a internet. 

[admin@mkt08] > ip pool add ranges=172.17.108.100-172.17.108.200 name=range_public
[admin@mkt08] > ip pool add ranges=172.17.208.100-172.17.208.200 name=range_private 
 

[admin@mkt08] > ip dhcp-server add interface=br-vlan108 address-pool=range_public 
[admin@mkt08] > ip dhcp-server add interface=br-vlan208 address-pool=range_private  

[admin@mkt08] > ip dhcp-server print 
Flags: X - disabled, I - invalid 
 #   NAME                             INTERFACE          RELAY           ADDRESS-POOL                            LEASE-TIME ADD-ARP
 0 X dhcp1                            br-vlan108                         range_public                            10m       
 1 X dhcp2                            br-vlan208                         range_private                           10m     

---probamos solo de la wifi(red puclica) y privada

[admin@mkt08] > ip dhcp-server set 0 disabled=no
[admin@mkt08] > ip dhcp-server set 1 disabled=no

[admin@mkt08] > ip dhcp-server print            
Flags: X - disabled, I - invalid 
 #   NAME                             INTERFACE         RELAY           ADDRESS-POOL                            LEASE-TIME ADD-ARP
 0   dhcp1                            br-vlan108                        range_public                            10m       
 1   dhcp2                            br-vlan208                        range_private                           10m       

----para sortir a internet getway default(nomes el de la publica)

[admin@mkt08] > ip dhcp-server network add gateway=172.17.108.1 dns-server=8.8.8.8 netmask=24 domain=publicoparatodos.com address=172.17.108.0/24--privada
---private
[admin@mkt08] > ip dhcp-server network add gateway=172.17.208.1 dns-server=8.8.8.8 netmask=24 domain=private.com address=172.17.208.0/24

[admin@mkt08] > ip dhcp-server network print 
 # ADDRESS            GATEWAY         DNS-SERVER      WINS-SERVER     DOMAIN                                                                         
 0 172.17.108.0/24    172.17.108.1    8.8.8.8                         publicoparatodos.com                                                           
 1 172.17.208.0/24    172.17.208.1    8.8.8.8                         private.com    																					



### [ejercicio6] Activar redes wifi y dar seguridad wpa2

[admin@mkt08] > interface wireless set wFree ssid=mktf08 
[admin@mkt08] > interface wireless set wPrivate  ssid=mktp08

[admin@mkt08] > interface wireless enable wFree 
[admin@mkt08] > interface wireless enable wPrivate 

[admin@mkt08] > interface wireless security-profiles add wpa2-pre-shared-key="pericolospalotes" name=perico


### [ejercicio6b] Opcional (montar portal cautivo)

### [ejercicio7] Firewall para evitar comunicaciones entre las redes Free y Private. Limitar
puertos en Free.

### [ejercicio8] Conexión a switch cisco con vlans

### [ejercicio8b] (Opcional) Montar punto de acceso adicional con un AP de otro fabricante

### [ejercicio9] QoS

### [ejercicio9] Balanceo de salida a internet

### [ejercicio10] Red de servidores con vlan de servidores y redirección de puertos

### [ejercicio11] Firewall avanzado

