EJERCICIO MIKROTIK 1.

OBJETIVO: FAMILIARIZARSE CON LAS OPERACIONES BÁSICAS EN UN ROUTER MIKROTIK

#1. Entrar al router con telnet, ssh y winbox

####TELNET######
$telnet 192.168.88.1
$login: admin
$passw:
[admin@MikroTik] > 

######SSH######
$ssh admin@192.168.88.1
$passwd:
[admin@MikroTik] > 

######WINBOX######


#2. Como resetear el route

[admin@MikroTik] > system reset               
Dangerous! Reset anyway? [y/N]: 

#3. Cambiar nombre del dispositivo, password 

[admin@MikroTik] > /system identity set name=infMKT08 

#4. Esquema de configuración de puertos inicial

[admin@infMKT08] > interface print 
Flags: D - dynamic, X - disabled, R - running, S - slave 
 #     NAME                                TYPE       ACTUAL-MTU L2MTU  MAX-L2MTU MAC-ADDRESS      
 0     ether1                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A1
 1  RS ether2-master                       ether            1500  1598       2028 6C:3B:6B:BC:BF:A2
 2  RS ether3                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A3
 3   S ether4                              ether            1500  1598       2028 6C:3B:6B:BC:BF:A4
 4   S wlan1                               wlan             1500  1600       2290 6C:3B:6B:BC:BF:A5
 5  R  ;;; defconf
       bridge                              bridge           1500  1598            6C:3B:6B:BC:BF:A2

---busca mes profundament(informacion especifica)

 [admin@infMKT08] > interface ethernet print 
Flags: X - disabled, R - running, S - slave 
 #    NAME                                  MTU MAC-ADDRESS       ARP        MASTER-PORT                                SWITCH                               
 0    ether1                               1500 6C:3B:6B:BC:BF:A1 enabled    none                                       switch1                              
 1 RS ether2-master                        1500 6C:3B:6B:BC:BF:A2 enabled    none                                       switch1                              
 2 RS ether3                               1500 6C:3B:6B:BC:BF:A3 enabled    ether2-master                              switch1                              
 3  S ether4                               1500 6C:3B:6B:BC:BF:A4 enabled    ether2-master                              switch1                              
 
----------------[admin@infMKT08] > interface ethernet export
		[admin@infMKT08] > interface ethernet

[admin@infMKT08] / interface ethernet> set [ find default-name=ether3 ] master-port=none<--change

#BRIDGE
[admin@infMKT08] > interface bridge print
[admin@infMKT08] > interface bridge print
[admin@infMKT08] > interface bridge port print 
Flags: X - disabled, I - inactive, D - dynamic 
 #    INTERFACE                                                     BRIDGE                                                     PRIORITY  PATH-COST    HORIZON
 0    ;;; defconf
      ether2-master                                                 bridge                                                         0x80         10       none
 1 I  ;;; defconf
      wlan1 
##veure interfaces wifi(normalment 1 wireless)

[admin@infMKT08] > interface wireless print 
Flags: X - disabled, R - running 
 0    name="wlan1" mtu=1500 l2mtu=1600 mac-address=6C:3B:6B:BC:BF:A5 arp=enabled interface-type=Atheros AR9300 mode=ap-bridge ssid="MikroTik-BCBFA5" 
      frequency=auto band=2ghz-b/g/n channel-width=20/40mhz-Ce scan-list=default wireless-protocol=802.11 vlan-mode=no-tag vlan-id=1 wds-mode=disabled 
      wds-default-bridge=none wds-ignore-ssid=no bridge-mode=enabled default-authentication=yes default-forwarding=yes default-ap-tx-limit=0 
      default-client-tx-limit=0 hide-ssid=no security-profile=default compression=no 

#cambiar la sdid a otro nombre
[admin@infMKT08] > /interface wireless set 0 ssid=MKT08

##Veure la wifi
[admin@infMKT08] > /interface wireless spectral-scan 0 
FREQ  DBM GRAPH                                            

#5. Cambiar nombres de puertos y deshacer configuraciones iniciales



#6. Backup y restauración de configuraciones


tutorial:
https://wiki.mikrotik.com/wiki/Configuration_Management_Spanish












