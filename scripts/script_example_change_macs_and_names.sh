!#/bin/bash
systemct stop NetworkManager

name_from_fedora_A=eth1
name_from_fedora_B=usb0
name_from_fedora_C=eth0

label_A=09
label_B=08
label_C=07

ip link set $name_from_fedora_A down
ip link set $name_from_fedora_B down
ip link set $name_from_fedora_C down

ip link set $name_from_fedora_A name usbA
ip link set $name_from_fedora_B name usbB
ip link set $name_from_fedora_C name usbC

ip link set usbA address aa:aa:aa:00:09:$label_A
ip link set usbB address aa:aa:aa:00:08:$label_B
ip link set usbC address aa:aa:aa:00:07:$label_C

ip link set usbA up
ip link set usbB up
ip link set usbC up
